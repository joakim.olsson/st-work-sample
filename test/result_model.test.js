const mongoose = require('mongoose');
const Result = require("../libs/result/result_model");

describe("Result model test", () => {
    beforeAll(async () => {
        await mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }, (err) => {
            if (err) {
                console.error(err);
                process.exit(1);
            }
        })
    });

    afterAll(async () => {
        await mongoose.connection.close();
    });

    it("has a module", () => {
        expect(Result).toBeDefined();
    })

    describe("save result", () => {
        it("saves result", async () => {
            const result = new Result({ event: 'start', horse: { id: 1, horse: "test_horse" }, time: 1 });
            await result.save();

            const foundResult = await Result.findOne({ "horse.horse": "test_horse" });
            expect("test_horse").toEqual(foundResult.horse.horse);
        })
    })
})