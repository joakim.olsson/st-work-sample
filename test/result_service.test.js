const ResultService = require('../libs/result/result_service');
const sinon = require('sinon');

describe("Result service test", () => {
    it("has a module", () => {
        expect(ResultService).toBeDefined();
    })

    describe("create result", () => {
        it("creates a result", async () => {
            const save = sinon.spy();
            let event, horse, time
            
            const MockModel = function (data) {
                event = data.event;
                horse = data.horse;
                time = data.time;

                return {
                    ...data,
                    save
                }
            };
            
            const resultService = ResultService(MockModel);
            resultService.createResult({ event: 'start', horse: { id: 1, horse: "test_horse" }, time: 1 })
            expect(save.calledOnce).toEqual(true);
            expect(event).toEqual('start');
            expect(horse).toEqual({ id: 1, horse: "test_horse" });
            expect(time).toEqual(1);
        })
    })
})