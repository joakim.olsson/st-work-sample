const Result = require('./result_model')

// Handle the result and store it on the database
 const createResult = Result => (result) => {
    const _result = new Result(result);
    _result.save((err) => {
        if (err) { 
            console.error('Could not save to db!'); 
        } else {
            console.log(`New result stored in db - ${JSON.stringify(result)}`)
        }
    });
}

module.exports = Result => ({
    createResult: createResult(Result)
});
