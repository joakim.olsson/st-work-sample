const Result = require("./result_model");
const ResultService = require("./result_service");

module.exports = ResultService(Result);
