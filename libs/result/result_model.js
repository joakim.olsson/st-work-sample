const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ResultSchema = new Schema({
    event: String,
    horse: {
        id: Number,
        horse:  String
    },
    time: Number
});

module.exports = mongoose.model('Result', ResultSchema);
