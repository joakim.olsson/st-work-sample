# Work test Svensk Travsport - Joakim Olsson, Mpya Digital
Runs a worker that polls the supplied REST API. The results fetched are stored in the given database in the collection `results`

## Setup and installation
Run `npm i` to install the required packages

## Run the worker
Run the worker by using the command `npm run start -- --db=DATABASE_ADDRESS` where `DATABASE_ADDRESS` is the address to your mongodb.

Example `npm run start -- --db=mongodb://127.0.0.1:27017/st`

## Tests
Run the tests by using the command `npm run test` (This will download mongoDB 4.2.0 the first time).