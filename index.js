const argv = require('yargs').argv
const request = require('request-promise');
const mongoose = require('mongoose');

// Import the result service
const ResultService = require('./libs/result')

// Verify that the database address is provided
if (!argv.db) {
    console.error('Missing database address\nUse the command "npm run start -- --db=DATABASE_ADDRESS"\n')
    process.exit();
}

// Start the worker
(async () => {
    console.log(`Connecting to database ${argv.db}`)
    try {
        // Connect to the database
        await mongoose.connect(argv.db, {
            useNewUrlParser: true, 
            useUnifiedTopology: true,
        })
    } catch(err) {
        console.error(`Could not connect to database at ${argv.db}\nError: ${err}`)
        process.exit();
    }
    console.log('Connected to database\nStarting polling results...')

    // Start polling results
    getResults();
})();

// Fetch the results from the server
async function getResults() {
    try {
        const result = await request('http://35.207.169.147/results', { json: true })
        if (result) {
            ResultService.createResult(result);
        }
    } catch(err) {
        console.log(err)
    } finally {
        // Once done/failed/timed out, call self to keep polling
        getResults();
    }
}
